from django.db import models

from apps.util.models import AbstractPersona, AbstractDireccion


class Ideologia(models.Model):
    class Meta:
        verbose_name = 'Ideología'
        verbose_name_plural = 'Ideologías'

    ideologia = models.CharField(max_length=100, blank=True, null=True)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return '%s' % (self.ideologia)


class Ocupacion(models.Model):
    class Meta:
        verbose_name = 'Ocupación'
        verbose_name_plural = 'Ocupaciones'

    nombre = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.nombre)


class Cargo(models.Model):
    class Meta:
        verbose_name = 'Cargo'
        verbose_name_plural = 'Cargos'

    nombre = models.CharField(max_length=100)
    decripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return '%s' % (self.nombre)


class Partido(AbstractDireccion):
    class Meta:
        verbose_name = 'Partido'
        verbose_name_plural = 'Partidos'

    COLOR = (
        #colores ?
    )
    nombre = models.CharField(max_length=200)
    fundacion = models.DateField(null=True, blank=True)
    colores = models.CharField(choices=COLOR, max_length=2, null=True, blank=True)
    
    web = models.URLField(null=True, blank=True)
    logo = models.ImageField(null=True, blank=True)

    #este atributo es para hacer una relacion para saber de que partido depende
    dependencia = models.ForeignKey(
        'self',
        null=True, blank=True,
        on_delete=models.CASCADE
    ) 
    ideologia = models.ManyToManyField(Ideologia)
    
    def __str__(self):
        return '%s' % (self.nombre)


class Politico(AbstractPersona):
    class Meta:
        verbose_name = 'Político'
        verbose_name_plural = 'Políticos'

    foto = models.ImageField(null=True, blank=True)
    descripcion = models.TextField(null=True, blank=True)

    partido = models.ForeignKey(Partido, on_delete=models.CASCADE)
    cargo = models.ForeignKey(Cargo, on_delete=models.CASCADE)
    ocupacion = models.ForeignKey(Ocupacion, on_delete=models.CASCADE)
    