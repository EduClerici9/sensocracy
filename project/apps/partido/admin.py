from django.contrib import admin

from . import models


admin.site.register(models.Ideologia)
admin.site.register(models.Ocupacion)
admin.site.register(models.Cargo)
admin.site.register(models.Partido)
admin.site.register(models.Politico)