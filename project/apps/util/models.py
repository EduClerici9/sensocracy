from django.db import models


class Provincia(models.Model):

    class Meta:
        ordering = ('nombre',)
        verbose_name_plural = ('Provincias')

    nombre = models.CharField(max_length=60,)

    def __str__(self):
        return self.nombre


class Departamento(models.Model):

    class Meta:
        ordering = ('nombre',)
        verbose_name_plural = ('Departamentos')

    nombre = models.CharField(max_length=60)
    provincia = models.ForeignKey(Provincia, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre


class Localidad(models.Model):

    class Meta:
        ordering = ('nombre',)
        verbose_name_plural = ('Localidades')

    nombre = models.CharField(max_length=100)
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    cod_postal = models.IntegerField(null=True, blank=True, default=None)

    def __str__(self):
        return self.nombre


class AbstractDireccion(models.Model):

    class Meta:
        abstract = True

    nombre_dir = models.CharField(
        'Nombre de Calle',
        max_length=100, 
        null=True, blank=True
    )
    numero_dir = models.PositiveIntegerField(
        'Número de Calle',
        null=True, blank=True
    )

    provincia = models.ForeignKey(
        Provincia, 
        null=True, blank=True, 
        on_delete=models.CASCADE
    )
    departamento = models.ForeignKey(
        Departamento,
        null=True, blank=True, 
        on_delete=models.CASCADE
    )
    localidad = models.ForeignKey(
        Localidad,
        null=True, blank=True,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return '{} {}'.format(self.nombre_dir, self.numero_dir)


class AbstractPersona(models.Model):

    class Meta:
        abstract = True

    SEXO = (
        ('F', 'Femenino'),
        ('M', 'Masculino'),
    )

    CIVIL = (
        ('CA', 'Casado/a'),
        ('CO', 'Comprometido/a'),
        ('DI', 'Divorciado/a'),
        ('SO', 'Soltero/a'),
        ('VI', 'Viudo/a'),

    )

    nombre = models.CharField('Nombre', max_length=100)
    apellido = models.CharField('Apellido', max_length=100)
    dni = models.CharField('DNI', max_length=8, unique=True)
    cuil = models.CharField('Cuil', max_length=13, unique=True)
    sexo = models.CharField(max_length=10, choices=SEXO)
    fecha_nacimiento = models.DateField('Fecha de nacimiento')

    @property
    def person_full_name(self):
        return '%s, %s' % (self.apellido, self.nombre)

    def __str__(self):
        return '%s' % (self.person_fullname)