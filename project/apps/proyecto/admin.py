from django.contrib import admin
from . import models


admin.site.register(models.Proyecto)
admin.site.register(models.Tipo)
admin.site.register(models.Tema)