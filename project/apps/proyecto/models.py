from django.db import models

from vote.models import VoteModel


class Tema(models.Model):

    nombre = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.nombre)


class Tipo(models.Model):

    nombre = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.nombre)


class Proyecto(VoteModel, models.Model):
    """
    Todos los proyectos de Ley a ser presentados
    """
    TIPO = (
        ('PL', 'Proyecto de Ley'),
    )

    ESTADO = (
        'EE', 'En Espera',
        'VO', 'Votando',
        'AP', 'Aprobado',
        'DE', 'Desaprobado'
    )
    nombre = models.CharField(max_length=200)
    expediente = models.CharField(max_length=20, null=True, blank=True)
    estado = models.CharField(choices=ESTADO, max_length=2, null=True, blank=True)
    iniciador = models.CharField(max_length=200)
    fecha_presentacion = models.DateField()
    fecha_votacion = models.DateField()
    descripcion = models.TextField(null=True, blank=True)

    tipo = models.ManyToManyField(Tipo)
    tema = models.ManyToManyField(Tema)
    
    def __str__(self):
        return '%s' % (self.nombre)