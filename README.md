Requeriments
============

Instalación
===========

Clone the repository:

```bash
$ git clone url...
```

Create and activate the virtual environment:

```bash
$ mkvirtualenv sensocracy
$ workon sensocracy
```

Install the requirements:

```bash
(sensocracy)$ pip install -r deploy/requirements.txt

```

Create the database and the database user:

```bash
$ sudo -u postgres psql
postgres=# CREATE ROLE sensocracy LOGIN PASSWORD 'sensocracy_zxcmnb' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
postgres=# CREATE DATABASE sensocracy WITH OWNER = sensocracy;
```
    
You have to create the file "settings/local.py" with your local database credentials:

```python
from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'sensocracy',
        'USER': 'sensocracy',
        'PASSWORD': 'sensocracy_zxcmnb',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
DEBUG = True

```
    
Initialize the database and set-up the Django environment:

```bash
(sensocracy)$ cd project/
(sensocracy)$ ./manage.py collectstatic
(sensocracy)$ ./manage.py makemigrations
(sensocracy)$ ./manage.py migrate
(sensocracy)$ ./manage.py createsuperuser
(sensocracy)$ ./manage.py runserver
```

